[![pipeline status](https://gitlab.com/wpdesk/predators/library/wp-octolize-helper-override/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/predators/library/wp-octolize-helper-override/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/predators/library/wp-octolize-helper-override/badges/master/coverage.svg)](https://gitlab.com/wpdesk/predators/library/wp-octolize-helper-override/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/predators/library/wp-octolize-helper-override/v/stable)](https://packagist.org/packages/wpdesk/predators/library/wp-octolize-helper-override) 
[![Total Downloads](https://poser.pugx.org/wpdesk/predators/library/wp-octolize-helper-override/downloads)](https://packagist.org/packages/wpdesk/predators/library/wp-octolize-helper-override) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/predators/library/wp-octolize-helper-override/v/unstable)](https://packagist.org/packages/wpdesk/predators/library/wp-octolize-helper-override) 
[![License](https://poser.pugx.org/wpdesk/predators/library/wp-octolize-helper-override/license)](https://packagist.org/packages/wpdesk/predators/library/wp-octolize-helper-override)

WordPress Library to override shared helper/tracker classes and share interoperability interfaces.
===================================================

This library should be always in composer's require and not require-dev.
As it is contains only abstractions and interfaces the tests are meaningless.