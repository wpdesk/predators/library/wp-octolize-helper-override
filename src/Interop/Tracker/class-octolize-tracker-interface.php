<?php

interface Octolize_Tracker_Interface {

	/**
	 * Setter for object that sends data.
	 *
	 * @param Octolize_Tracker_Sender $sender Object that can send payloads.
	 */
	public function set_sender( Octolize_Tracker_Sender $sender );

	/**
	 * Attach data provider class to tracker
	 *
	 * @param Octolize_Tracker_Data_Provider $provider
	 *
	 * @return void
	 */
	public function add_data_provider( Octolize_Tracker_Data_Provider $provider );
}
