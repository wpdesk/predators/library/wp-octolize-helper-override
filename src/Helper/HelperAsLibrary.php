<?php

namespace Octolize\Helper;

/**
 * @deprecated Do not use. Only for purpose of compatibility with library 1.x version
 *
 * @package Octolize\Helper
 */
class HelperAsLibrary
{
    public function hooks()
    {
        do_action('octolize_helper_instance');
    }
    /**
     * @return \Octolize_Tracker
     */
    public function get_tracker()
    {
        return apply_filters('octolize_tracker_instance', null);
    }
    /**
     * @return LoggerInterface
     */
    public function get_logger()
    {
        return apply_filters('octolize_logger_instance', null);
    }
}
