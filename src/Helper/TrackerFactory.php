<?php
/**
 * @deprecated Do not use. Only for purpose of compatibility with library 1.x version
 *
 * Class Octolize_Tracker_Factory
 */
class Octolize_Tracker_Factory
{
    /**
     * Creates tracker instance.
     *
     * @param string $basename Plugin basename.
     *
     * @return Octolize_Tracker created tracker.
     */
    public function create_tracker($basename)
    {
        return apply_filters('octolize_tracker_instance', null);
    }
}